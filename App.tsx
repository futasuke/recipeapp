import 'react-native-gesture-handler';

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Provider } from "mobx-react";

// React-navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

// Custom
import AppStore from './state/store';

// Screen View Component
import Welcome from './views/Welcome';
import Home from './views/Home';
import RecipeDetail from './views/RecipeDetail';
import NewRecipe from './views/NewRecipe';

const MainStack = createStackNavigator();
const MainDrawer = createDrawerNavigator();
const store = window.store = new AppStore();

export default class App extends React.Component {

  homeDrawer(props) {
    let thisRoute = props.route;

    return (
      <Provider store={store}>
        <MainDrawer.Navigator>
          <MainDrawer.Screen name="HomeScreen" component={Home} />
          <MainDrawer.Screen name="NewRecipe" component={NewRecipe} />
        </MainDrawer.Navigator>
      </Provider>
    )
  }

  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <MainStack.Navigator headerMode={'none'}>
            <MainStack.Screen name="WelcomeScreen" component={Welcome} />
            <MainStack.Screen name="HomeDrawer" component={this.homeDrawer} />
            <MainStack.Screen name="RecipeDetail" component={RecipeDetail} />
          </MainStack.Navigator>
        </NavigationContainer>
      </Provider>
    )
  }
}
