import {observable} from 'mobx'

class AppStore{
    @observable ip = "http://192.168.0.105:8080"
    @observable recipeType = [];
}

export default AppStore