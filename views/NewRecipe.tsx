import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    Alert,
    Modal,
} from 'react-native';
import { observer, inject } from "mobx-react";
import axios from 'axios';
import { color } from 'react-native-reanimated';
import { launchImageLibrary } from 'react-native-image-picker';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

@inject('store')
@observer
export default class NewRecipe extends React.Component {
    state = {
        name: "",
        type: "Recipe Type",
        ingredients: "",
        steps: "",
        showModal: false,
        filename:"Select Image",
        fileType:"",
        base64:"",
    }

    submitForm() {
        if (this.state.name == "" || this.state.type == "Recipe Type" || this.state.ingredients == "" || this.state.steps == "") {
            Alert.alert("Please fill in all inputs");
            return;
        }

        console.log(this.state);

        let self = this;
        axios.post(this.props.store.ip + "/api/add-data", {
            name: this.state.name,
            type: this.state.type,
            ingredients: this.state.ingredients,
            steps: this.state.steps,
            filename:this.state.filename,
            filetype:this.state.fileType,
            base64:this.state.base64,
        }).then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
            console.log(error.response);
        })
    }

    renderFilterList() {
        let self = this;

        return this.props.store.recipeType.map(function (data, index) {
            return (
                <TouchableOpacity key={index} onPress={() => { self.selectType(data.name) }} style={[styles.recipeType, { marginTop: index > 0 ? 10 : 0 }]}>
                    <Text>{data.name}</Text>
                </TouchableOpacity>
            )
        })
    }

    selectType(type) {
        this.setState({ type: type, showModal: false });
    }

    openImagePicker(){
        let self=this;
        console.log("Open image picker");
        const options={
            mediaType:'photo',
            includeBase64:true,
        }
        launchImageLibrary(options,(response)=>{
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
                return;
            }
            let tmpType = response.type.split("/");
            this.setState({filename:response.fileName,fileType:tmpType[1],base64:response.base64});
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView style={{ backgroundColor: "#dfdfdf" }}>
                    <View style={styles.navigation}>
                        <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => this.props.navigation.toggleDrawer()}>
                            <Image source={require("../assets/menu_bar.png")} style={{ width: 30, height: 30 }} />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontWeight: "bold", fontSize: 22, color: "white" }}>Add New Recipe</Text>
                        </View>
                        <View style={{ paddingRight: 15 }}>
                            <Image source={require("../assets/menu_bar_dummy.png")} style={{ width: 30, height: 30 }} />
                        </View>
                    </View>

                    <View style={styles.body}>

                        {/* Recipe Image */}
                        <View style={styles.sectionContainer}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Recipe Name</Text>
                            <TouchableOpacity style={[styles.inputContainer, { paddingTop: 15, paddingBottom: 15 }]} onPress={()=>this.openImagePicker()}>
                                <Text>{this.state.filename}</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Recipe name */}
                        <View style={styles.sectionContainer}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Recipe Name</Text>
                            <View style={styles.inputContainer}>
                                <TextInput placeholder="Recipe Name" style={{ padding: 0 }} onChangeText={value => this.setState({ name: value })} />
                            </View>
                        </View>

                        {/* Recipe type */}
                        <View style={styles.sectionContainer}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Recipe Type</Text>
                            <TouchableOpacity style={[styles.inputContainer, { paddingTop: 15, paddingBottom: 15 }]} onPress={() => { this.setState({ showModal: true }) }}>
                                <Text style={{ color: this.state.type == "Recipe Type" ? "grey" : "black" }}>{this.state.type}</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Recipe type modal */}
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={this.state.showModal}
                        // visible={true}
                        >
                            <View style={styles.modalContainer}>
                                <View style={styles.modalContent}>
                                    {this.renderFilterList()}
                                </View>
                            </View>
                        </Modal>

                        {/* Recipe ingredients */}
                        <View style={styles.sectionContainer}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Recipe Ingredients</Text>
                            <View style={styles.inputContainer}>
                                <TextInput placeholder="Recipe Ingredients" style={{ padding: 0 }} multiline={true} onChangeText={value => this.setState({ ingredients: value })} />
                            </View>
                        </View>

                        {/* Recipe steps */}
                        <View style={styles.sectionContainer}>
                            <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Recipe Steps</Text>
                            <View style={styles.inputContainer}>
                                <TextInput placeholder="Recipe Steps" style={{ padding: 0 }} multiline={true} onChangeText={value => this.setState({ steps: value })} />
                            </View>
                        </View>

                        <TouchableOpacity style={styles.submitButton} onPress={() => this.submitForm()}>
                            <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>SUBMIT</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navigation: {
        flex: 1,
        backgroundColor: "#3f99e9",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10,
    },
    body: {
        flex: 12,
        backgroundColor: "#dfdfdf",
        alignItems: 'center',
    },
    sectionContainer: {
        width: screenWidth - 20,
        marginTop: 20,
    },
    inputContainer: {
        backgroundColor: "white",
        borderRadius: 10,
        padding: 10,
        marginTop: 10
    },
    submitButton: {
        backgroundColor: "#3f99e9",
        marginTop: 30,
        marginBottom: 100,
        width: screenWidth - 100,
        padding: 15,
        borderRadius: 20,
        justifyContent: "center",
        alignItems: 'center',
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: screenWidth / 1.5,
        height: screenHeight / 4,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 20,
    },
    recipeType: {
        padding: 10,
        borderWidth: 1,
        borderColor: "grey",
        borderRadius: 20,
    }
});