import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
    Modal
} from 'react-native';
import axios from 'axios';
import { observer, inject } from "mobx-react";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

@inject('store')
@observer
class Home extends React.Component {
    state = {
        recipeList: [],
        filterName:"Filter",
        showModal:false,
        // type:"",
    }

    componentDidMount() {
        let self = this;
        this.getAllRecipe();
        axios.get(this.props.store.ip + "/api/get-all-type").then(function (response) {
            self.props.store.recipeType = response.data;
        }).catch(function (error) {
            console.log(error)
        })
        this.onFocus = this.props.navigation.addListener('focus', () => {
            self.getAllRecipe();
        });
    }

    getAllRecipe(){
        let self = this;
        axios.get(this.props.store.ip + "/api/get-all-recipe").then(function (response) {
            self.setState({ recipeList: response.data, filterName:"Filter" })
        }).catch(function (error) {
            console.log(error.response);
        })
    }

    renderRecipeList() {
        let self = this;

        return this.state.recipeList.map(function (data, index) {
            let tmpDate = data.created_at.split("T")[0];

            return (
                <TouchableOpacity key={index} style={[styles.cardContainer, { marginTop: index > 1 ? 20 : 0 }]} onPress={()=>self.props.navigation.navigate("RecipeDetail",{recipeData:data})}>
                    <Image source={{uri:self.props.store.ip + data.imagePath,cache: 'reload'}} style={{ width: 150, height: 150 }} borderRadius={5} />
                    <View style={{ marginTop: 10 }}>
                        <Text style={{ fontSize: 18, fontWeight: "bold" }}>{data.name}</Text>
                        <View style={{ borderTopColor: "grey", borderTopWidth: 1, marginTop: 10, paddingTop: 10 }}>
                            <Text>Created at : {tmpDate}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    renderFilterList(){
        let self = this;
        
        return this.props.store.recipeType.map(function(data,index){
            return(
                <TouchableOpacity key={index} onPress={()=>{self.requestFilteredRecipe(data.name)}} style={[styles.recipeType,{marginTop:index>0?10:0}]}>
                    <Text>{data.name}</Text>
                </TouchableOpacity>
            )
        })
    }

    requestFilteredRecipe(type){
        let self = this;
        this.setState({showModal:false,filterName:type});
        axios.get(this.props.store.ip + "/api/get-recipes/filter/"+ type).then(function(response){
            self.setState({ recipeList: response.data })
        }).catch(function(error){
            console.log(error);
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>

                {/* Filter modal */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showModal}
                    // visible={true}
                >
                    <View style={styles.modalContainer}>
                        <View style={styles.modalContent}>
                            {this.renderFilterList()}
                        </View>
                    </View>
                </Modal>

                <View style={styles.navigation}>
                    <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => this.props.navigation.toggleDrawer()}>
                        <Image source={require("../assets/menu_bar.png")} style={{ width: 30, height: 30 }} />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontWeight: "bold", fontSize: 22, color: "white" }}>RecipeApp</Text>
                    </View>
                    <View style={{ paddingRight: 15 }}>
                        <Image source={require("../assets/menu_bar_dummy.png")} style={{ width: 30, height: 30 }} />
                    </View>
                </View>
                <View style={styles.body}>
                    <ScrollView>
                        <TouchableOpacity style={styles.filterBox} onPress={()=>this.setState({showModal:true})}>
                            <View style={{ flexDirection: 'row', justifyContent: "center", alignItems: "center" }}>
                                <Image source={require("../assets/search_blue.png")} style={{ width: 30, height: 30, marginRight: 20 }} />
                                <Text>{this.state.filterName}</Text>
                            </View>
                            <View>
                                <Image source={require("../assets/caret_down_blue.png")} style={{ width: 30, height: 30 }} />
                            </View>
                        </TouchableOpacity>
                        <View style={styles.contentContainer}>
                            <View style={{ marginLeft: 20,marginRight:20,flexDirection:"row", alignItems:"center",justifyContent:"space-between" }}>
                                <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>List of Recipes</Text>
                                <TouchableOpacity style={{backgroundColor:"#3f99e9",padding:10,borderRadius:20}} onPress={()=>this.getAllRecipe()}>
                                    <Text style={{color:"white",fontWeight:"bold"}}>Clear Filter</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.content}>
                                {/* In loop */}
                                {this.renderRecipeList()}
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navigation: {
        flex: 1,
        backgroundColor: "#3f99e9",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    body: {
        flex: 13,
        backgroundColor: "#dfdfdf",
    },
    filterBox: {
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
    },
    contentContainer: {
        marginTop: 20,
        marginBottom:50,
    },
    content: {
        // backgroundColor: "white",
        width: screenWidth - 20,
        alignSelf: "center",
        marginTop: 25,
        flexDirection: "row",
        justifyContent: "space-around",
        flexWrap: "wrap",
    },
    cardContainer: {
        backgroundColor: "white",
        padding: 5,
        borderRadius: 5,
        // flexGrow:1,
        // flex:10,
        // alignItems:"center",
    },
    modalContainer:{
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent:{
        width:screenWidth/1.5,
        height:screenHeight/4,
        backgroundColor:"white",
        padding:10,
        borderRadius:20,
    },
    recipeType:{
        padding:10,
        borderWidth:1,
        borderColor:"grey",
        borderRadius:20,
    }
})

export default Home;