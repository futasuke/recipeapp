import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
    Modal,
} from 'react-native';
import axios from 'axios';
import { TextInput } from 'react-native-gesture-handler';
import { observer, inject } from "mobx-react";
import { launchImageLibrary } from 'react-native-image-picker';

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

@inject('store')
@observer
export default class RecipeDetail extends React.Component {
    state = {
        recipeData: this.props.route.params.recipeData,
        name: this.props.route.params.recipeData.name,
        ingredients: this.props.route.params.recipeData.ingredients,
        steps: this.props.route.params.recipeData.steps,
        showModal: false,
        id: this.props.route.params.recipeData.id,
        modalTitle: "",
        modalValue: "",
        filename:"",
        filetype:"",
        base64:"",
    }

    componentDidMount() {
        console.log(this.props);
    }

    updateRecipe() {
        this.setState({ showModal: false })
        let self = this;
        axios.post(this.props.store.ip + "/api/update-data", {
            name: this.state.name,
            ingredients: this.state.ingredients,
            steps: this.state.steps,
            id: this.state.id
        }).then(function (response) {
            console.log(response);
            self.props.navigation.navigate('HomeScreen');
        }).catch(function (error) {
            console.log(error);
        })
    }

    deleteRecipe() {
        let self = this;
        axios.get(this.props.store.ip + "/api/delete-recipe/" + this.state.id).then(function (response) {
            console.log(response);
            self.props.navigation.navigate('HomeScreen');
        }).catch(function (error) {
            console.log(error);
        })
    }

    selectImage() {
        let self=this;
        console.log("Open image picker");
        const options={
            mediaType:'photo',
            includeBase64:true,
        }
        launchImageLibrary(options,(response)=>{
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
                return;
            }
            let tmpType = response.type.split("/");
            this.setState({filename:response.fileName,filetype:tmpType[1],base64:response.base64});
            this.uploadImage();
        })
    }

    uploadImage(){
        let self = this;
        // console.log(this.state.filetype)
        axios.post(this.props.store.ip + "/api/update-image",{
            id:this.state.id,
            name:this.state.name,
            filename:this.state.filename,
            filetype:this.state.filetype,
            base64:this.state.base64,
        }).then(function(response){
            console.log(response)
            self.props.navigation.navigate('HomeScreen');
        }).catch(function(error){
            console.log(error.response)
        })
    }

    openModal(title) {
        this.setState({ showModal: true, modalTitle: title })
    }

    renderModalContent() {
        let self = this;

        if (this.state.modalTitle == "Name") {
            return (
                <View>
                    <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>{this.state.modalTitle}</Text>
                    <TextInput defaultValue={self.state.name} onChangeText={value => this.setState({ name: value })}></TextInput>
                </View>
            )
        }
        if (this.state.modalTitle == "Ingredients") {
            return (
                <View>
                    <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>{this.state.modalTitle}</Text>
                    <TextInput multiline={true} defaultValue={self.state.ingredients} onChangeText={value => this.setState({ ingredients: value })}></TextInput>
                </View>
            )
        }
        if (this.state.modalTitle == "Steps") {
            return (
                <View>
                    <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>{this.state.modalTitle}</Text>
                    <TextInput multiline={true} defaultValue={self.state.steps} onChangeText={value => this.setState({ steps: value })}></TextInput>
                </View>
            )
        }

    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={{ backgroundColor: "#dfdfdf" }}>

                    {/* Update modal */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.showModal}
                    >
                        <View style={styles.modalContainer}>
                            <View style={styles.modalContent}>
                                <ScrollView>
                                    {this.renderModalContent()}
                                </ScrollView>
                                <TouchableOpacity onPress={() => this.updateRecipe()}>
                                    <Text>SUBMIT</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>

                    <View style={styles.navigation}>
                        <TouchableOpacity style={{ paddingLeft: 15 }} onPress={() => this.props.navigation.goBack()}>
                            <Image source={require("../assets/chevronleft.png")} style={{ width: 30, height: 30 }} />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontWeight: "bold", fontSize: 22, color: "white" }}>RecipeApp</Text>
                        </View>
                        <TouchableOpacity style={{ paddingRight: 15 }} onPress={() => this.deleteRecipe()}>
                            <Image source={require("../assets/trash.png")} style={{ width: 30, height: 30 }} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.body}>
                        <View style={{ marginTop: 10 }}>
                            <TouchableOpacity onPress={()=>this.selectImage()}>
                                <Image source={{ uri: this.props.store.ip + this.state.recipeData.imagePath }} style={{ width: screenWidth, height: 150 }} />
                            </TouchableOpacity>
                        </View>

                        <View style={{ marginTop: 25, paddingLeft: 10, paddingRight: 10 }}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Name</Text>
                                <TouchableOpacity onPress={() => this.openModal("Name")}>
                                    <Image source={require("../assets/edit_blue.png")} style={{ width: 30, height: 30 }} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.sectionContainer}>
                                <Text>{this.state.recipeData.name}</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: 25, paddingLeft: 10, paddingRight: 10 }}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Ingredients</Text>
                                <TouchableOpacity onPress={() => this.openModal("Ingredients")}>
                                    <Image source={require("../assets/edit_blue.png")} style={{ width: 30, height: 30 }} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.sectionContainer}>
                                <Text>{this.state.recipeData.ingredients}</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: 25, paddingLeft: 10, paddingRight: 10 }}>
                            <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={{ fontSize: 18, fontWeight: "bold", color: "#5f5f5f" }}>Steps</Text>
                                <TouchableOpacity onPress={() => this.openModal("Steps")}>
                                    <Image source={require("../assets/edit_blue.png")} style={{ width: 30, height: 30 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.sectionContainer}>
                                <Text>{this.state.recipeData.steps}</Text>
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navigation: {
        flex: 1,
        backgroundColor: "#3f99e9",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10,
    },
    body: {
        flex: 13,
        backgroundColor: "#dfdfdf",
    },
    sectionContainer: {
        backgroundColor: "white",
        padding: 10,
        borderRadius: 20,
        marginTop: 10,
    },
    modalContainer: {
        backgroundColor: 'rgba(0,0,0,0.3)',
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: 'column',
    },
    modalContent: {
        width: screenWidth / 1.5,
        height: screenHeight / 3.5,
        backgroundColor: "white",
        padding: 10,
        borderRadius: 20,
    },
})