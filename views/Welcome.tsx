import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    Dimensions,
    TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import {observer,inject} from "mobx-react";

const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const fryingPanRatio = screenWidth / 728;

@inject('store')
@observer
export default class Welcome extends React.Component {

    goToHome() {
        this.props.navigation.navigate("HomeDrawer");
    }

    componentDidMount() {
        console.log(this.props);
    }

    render() {
        return (
            <SafeAreaView>
                <ScrollView>

                    <View style={styles.upperPart}>
                        <View>
                            <Image source={require('../assets/fryingpan.png')} style={{ width: screenWidth, height: 744 * fryingPanRatio / 2 }} resizeMode="contain" />
                        </View>
                    </View>

                    <View style={styles.lowerPart}>
                        <View>
                            <Text style={{ fontSize: 30 }}>WELCOME</Text>
                        </View>
                        <TouchableOpacity style={styles.enterButton} onPress={() => this.goToHome()}>
                            <Text style={{ fontSize: 20, color: "white", fontWeight: "bold" }}>ENTER</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    upperPart: {
        paddingTop: 50,
        paddingBottom: 50,
        backgroundColor: "#3f99e9",
        borderBottomLeftRadius: 150,
        borderBottomRightRadius: 150,
    },
    lowerPart: {
        marginTop: 100,
        alignItems: "center"
    },
    enterButton: {
        backgroundColor: "#3f99e9",
        width: screenWidth / 2,
        justifyContent: "center",
        alignItems: "center",
        padding: 25,
        borderRadius: 25,
        marginTop: 45,
    }
})